package it.unipd.dei.dpms.detailfeature;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    private static final String TAG = "Details Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        getIncomingIntent();
    }

    private void getIncomingIntent() {
        Log.d(TAG, "getIncomingIntent: checking for incoming intents.");

        //Se l'intent possiede tutte le informazioni da caricare
        if ((getIntent().hasExtra("sku"))
                && (getIntent().hasExtra("nome"))
                && (getIntent().hasExtra("prezzo"))
                && (getIntent().hasExtra("colore"))
                && (getIntent().hasExtra("descrizione"))
                && (getIntent().hasExtra("immagine"))) {
            Log.d(TAG, "getIncomingIntent: found intent extras.");
            //prendo tutto quello che mi serve sapere e mostrare
            String sku = getIntent().getStringExtra("sku");
            String nome = getIntent().getStringExtra("nome");
            Integer prezzo = getIntent().getIntExtra("prezzo", 0);

            String colore = getIntent().getStringExtra("colore");
            String descrizione = getIntent().getStringExtra("descrizione");

            //controllare che problemi ha che nonn carica il codice dell'immagine
            Integer immagine = getIntent().getIntExtra("immagine", 0);

            //e creo la vista
            setView(sku, nome, prezzo, colore, descrizione, immagine);
        }
    }
    private void setView(String sku, String nome, Integer prezzo,
                         String colore, String descrizione,
                         Integer immagine){
        //sistemare log
        Log.d(TAG, "setImage: setting te image and name to widgets.");

        TextView vSku = findViewById(R.id.skud);
        vSku.setText(sku);
        TextView vNome = findViewById(R.id.nomed);
        vNome.setText(nome);
        TextView vPrezzo = findViewById(R.id.prezzod);
        vPrezzo.setText(prezzo.toString()+",00€");
        TextView vColore = findViewById(R.id.colored);
        vColore.setText(colore);
        TextView vDescrizione = findViewById(R.id.descrizioned);
        vDescrizione.setText(descrizione);

        ImageView vImmagine = (ImageView) findViewById(R.id.immagine);
        vImmagine.setImageResource(immagine);

    }


}
