package it.unipd.dei.dpms.base;

public class Product {
    private String sku, nome, categoria, descrizione, colore;
    private Integer prezzo, IDthumb, IDimage;

    public Product(){}

    public Product(String sku, String nome, String categoria, Integer prezzo, Integer IDthumb,String colore, String descrizione, Integer IDimage){
        this.sku = sku;
        this.nome = nome;
        this.categoria = categoria;
        this.prezzo = prezzo;
        this.IDthumb = IDthumb;
        this.colore = colore;
        this.descrizione = descrizione;
        this.IDimage = IDimage;
    }

    public String getSku(){
        return sku;
    }
    public String getNome(){
        return nome;
    }
    public String getCategoria() {
        return categoria;
    }
    public Integer getPrezzo() {
        return prezzo;
    }
    public void setSku(String sku) {
        this.sku = sku;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
    public void setPrezzo(Integer prezzo) {
        this.prezzo = prezzo;
    }
    public Integer getIDthumb() { return IDthumb; }
    public void setIDthumb(Integer IDimage) { this.IDthumb = IDthumb; }
    public String getDescrizione() { return descrizione; }
    public void setDescrizione(String descrizione) { this.descrizione = descrizione; }
    public String getColore() { return colore; }
    public void setColore(String colore) { this.colore = colore; }
    public Integer getIDimage() { return IDimage; }
    public void setIDimage(Integer IDimage) { this.IDimage = IDimage; }
}
