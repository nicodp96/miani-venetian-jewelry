package it.unipd.dei.dpms.mianivenetianjewelry.main;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import it.unipd.dei.dpms.base.Product;
import it.unipd.dei.dpms.mianivenetianjewelry.R;

import static android.content.ContentValues.TAG;


//classe praticamente non commentata perchè ci si mette di più a spiegarla che a leggere la documentazione


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {


    private List<Product> productList;
    private Context context;

    //costruttore
    public ProductAdapter(Context context, List<Product> productList) {
        this.context = context;
        this.productList = productList;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView sku, nome, categoria, prezzo;
        public ImageView thumbnail;
        RelativeLayout relativeLayout;

        public MyViewHolder(View view) {
            super(view);

            sku = (TextView) view.findViewById(it.unipd.dei.dpms.mianivenetianjewelry.main.R.id.sku);
            nome = (TextView) view.findViewById(it.unipd.dei.dpms.mianivenetianjewelry.main.R.id.nome);
            //categoria = (TextView) view.findViewById(R.id.categoria);
            prezzo = (TextView) view.findViewById(it.unipd.dei.dpms.mianivenetianjewelry.main.R.id.prezzo);
            thumbnail = (ImageView) view.findViewById(it.unipd.dei.dpms.mianivenetianjewelry.main.R.id.thumbnail);
            //serve dopo per passare all'activity successiva
            relativeLayout = itemView.findViewById(it.unipd.dei.dpms.mianivenetianjewelry.main.R.id.relative_layout);
        }

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(it.unipd.dei.dpms.mianivenetianjewelry.main.R.layout.product_list_row, parent, false);
        return new MyViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Product product = productList.get(position);
        Log.d(TAG, "holder at position " + position + " is " + holder);
        holder.sku.setText(product.getSku());
        holder.nome.setText(product.getNome());
        //holder.categoria.setText(product.getCategoria());
        //considerare suggerimento per riga sotto
        holder.prezzo.setText(Integer.toString(product.getPrezzo())+",00€");

        //setta ogni volta la risorsa in base al nome(=id della risorsa) salvato in prodotto
        holder.thumbnail.setImageResource(product.getIDthumb());

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i(TAG, "Apertura dettagli");
                Toast.makeText(context, "Apertura dettagli", Toast.LENGTH_SHORT).show();

                // Intent intent = new Intent(context, it.unipd.dei.dpms.detailfeature.DetailsActivity);
                Intent intent = null;
                try {
                    intent = new Intent(context, Class.forName("it.unipd.dei.dpms.detailfeature.DetailsActivity"));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                //aggiungo all'intent le informazioni che mi serviranno
                intent.putExtra("sku",productList.get(position).getSku());
                intent.putExtra("nome",productList.get(position).getNome());
                intent.putExtra("prezzo",productList.get(position).getPrezzo());
                intent.putExtra("colore",productList.get(position).getColore());
                intent.putExtra("descrizione",productList.get(position).getDescrizione());
                intent.putExtra("immagine",productList.get(position).getIDimage());
                //throws android.content.ActivityNotFoundException
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

}
