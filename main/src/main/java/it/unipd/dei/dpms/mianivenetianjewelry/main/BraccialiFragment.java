package it.unipd.dei.dpms.mianivenetianjewelry.main;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class BraccialiFragment extends Fragment {

    private RecyclerView recyclerView;
    private ProductAdapter mAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //just change the fragment_dashboard
        //with the fragment you want to inflate
        //like if the class is HomeFragment it should have R.layout.home_fragment
        //if it is DashboardFragment it should have R.layout.fragment_dashboard
        View view = inflater.inflate(R.layout.fragment_bracciali, null);

        //prova di recyclerview

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mAdapter = new ProductAdapter(getContext(),ProductListActivity.listaBracciali);

        //si deve impostare la RecyclerView caricando l'Adapter mAdapter
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        //Sets the RecyclerView.ItemAnimator that will handle animations involving changes to the items in this RecyclerView.
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //facciamo un po' i fighi con i separatori tra le tuple ahah
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL));

        recyclerView.setAdapter(mAdapter);

        return view;
    }
}