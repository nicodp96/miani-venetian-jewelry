package it.unipd.dei.dpms.mianivenetianjewelry.main;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import it.unipd.dei.dpms.base.Product;

import static com.google.android.instantapps.InstantApps.isInstantApp;
import static com.google.android.instantapps.InstantApps.showInstallPrompt;

public class ProductListActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    //variabili necessarie per il caricamento del database

    public static List<Product> productList = new ArrayList<>();

    public static List<Product> listaCollane = new ArrayList<>();
    public static List<Product> listaBracciali = new ArrayList<>();
    public static List<Product> listaOrecchini = new ArrayList<>();

    private RecyclerView recyclerView;
    private ProductAdapter mAdapter;

    //per estrapolare dati dal database
    private String sku;
    private String nome;
    private String categoria;
    private Integer prezzo;
    private Integer IDthumb;
    private String descrizione;
    private String colore;
    private Integer IDimage;

    private String nome_thumbnail;
    private String nome_immagine;


    public static final String TABLE_NAME = "catalogo";
    public static final String DATABASE_NAME = "catalogointero.db";

    public String TAG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        //loading la topbar

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //loading the default fragment
        loadFragment(new CollaneFragment());
        Log.i(TAG, "Problema sul load dei fragment");

        //getting bottom navigation view and attaching the listener
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);
        Log.i(TAG, "Problema sul load della bottom bar");

        //free the list
        productList.clear();

        //caricamento database
        loadDataBase();
        Log.i(TAG, "Problema sul load del database");

        //free every category list
        listaCollane.clear();
        listaBracciali.clear();
        listaOrecchini.clear();

        //seleziono gli elementi da mettere delle liste di ciascuna categoria
        for(int i=0; i<productList.size();i++){
            switch (productList.get(i).getCategoria()) {
                case "Collana":
                    listaCollane.add(productList.get(i));
                    break;
                case "Bracciale":
                    listaBracciali.add(productList.get(i));
                    break;
                case "Orecchini":
                    listaOrecchini.add(productList.get(i));
            }
        }
    }

    //fragments navigation
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        if(item.getItemId()==R.id.category_collane) {
            fragment = new CollaneFragment();
        }
        else if (item.getItemId()==R.id.category_bracciali){
            fragment = new BraccialiFragment();
        }
        else if(item.getItemId()==R.id.category_orecchini){
            fragment = new OrecchiniFragment();
        }
      /*  switch (item.getItemId()) {
            case R.id.category_collane:
                fragment = new CollaneFragment();
                break;

            case R.id.category_bracciali:
                fragment = new BraccialiFragment();
                break;

            case R.id.category_orecchini:
                fragment = new OrecchiniFragment();
                break;
        }*/

        return loadFragment(fragment);
    }

    //fragments loading
    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }


    //adding items to the topbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.topbar, menu);
        return true;
    }

    //favorite button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.action_favorite) {
            //code to change activity, i hope

            //Se si naviga con la instant si può far uscire una finestra con l'ipotetico
            //link allo store per scaricare la app

            if(isInstantApp(this)){
                String title = "Attenzione";
                String message = "Se si vuole accedere a questa funzione scaricare l'applicazione dal Play Store";
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(true);
                builder.setTitle(title);
                builder.setMessage(message);
                builder.show();

            }
            else{
                Intent intent;

                try {
                    intent = new Intent(this, Class.forName("it.unipd.dei.dpms.favoritefeature.FavoriteListActivity"));
                    startActivity(intent);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                //public static boolean showInstallPrompt(Activity activity, Intent postInstallIntent, int requestCode, String referrer)

            }
            return true;
        }
        else
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            return super.onOptionsItemSelected(item);

       /* switch (item.getItemId()) {
            case R.id.action_favorite:
                //code to change activity, i hope
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        } */
    }

    //utilizzato per salvare lo stato dei fragment

    // Documentazione:
    // This callback is called only when there is a saved instance that is previously saved by using
    // onSaveInstanceState(). We restore some state in onCreate(), while we can optionally restore
    // other state here, possibly usable after onStart() has completed.
    // The savedInstanceState Bundle is same as the one used in onCreate().

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onCreate(savedInstanceState);
    }

    private void loadDataBase() {
        //caricamento database
        SQLiteDatabase db = null;
        Integer el_num; //salva il numero di elementi
        Cursor res;

        File dbFile = this.getDatabasePath(DATABASE_NAME);

        try {
            if (!dbFile.getParentFile().exists() && !dbFile.getParentFile().mkdir()) {
                throw new IOException("couldn't make databases directory");
            }
            copyDataBase(dbFile);
            db = SQLiteDatabase.openDatabase(String.valueOf(dbFile),null, SQLiteDatabase.OPEN_READONLY);
        }catch(IOException e) {
            Log.e(TAG, "Problema nel caricamento del database, viene lanciato IOException");
        }


        try {
            res = db.rawQuery("select * from " + TABLE_NAME, null);
        }catch(NullPointerException e){
            Log.d(TAG,"rawQuery fallito, res impostato a null, verrà lanciata un'altra eccezione");
            res = null;
        }

        //controllo per vedere se in Cursor è stato salvato qualcosa
        //(ennesimo controllo per il caricamento del db)
        //se sì il database è stato caricato
        try {
            el_num = res.getCount();
        }catch(NullPointerException e){
            Log.d(TAG,"NullPointerException nel contare i dati contenuti nel risultato di " +
                    "SELECT * FROM catalogo");
            el_num=0;
        }
        if(el_num==0){
            Log.e(TAG,"Cursor vuoto");
        }
        else {

            while (res.moveToNext()) {
                //va esaminata ogni tupla per estrarre il valore di ogni colonna così da
                //creare un oggetto Product che andrà aggiunto alla lista ProductList

                createProductList(res);

            }

        }
    }
    private void createProductList(Cursor res){

        //metodo utilizzato per creare la lista di prodotti

        Product product;

        sku = res.getString(0);
        nome = res.getString(1);
        categoria = res.getString(2);
        prezzo = res.getInt(3);
        descrizione = res.getString(6);
        colore = res.getString(5);

        //estrae il nome e ricava l'id della risorsa
        nome_thumbnail = res.getString(4);
        //controllare che i nomi delle immagini siano scritte in minuscolo
        IDthumb = getResources().getIdentifier(nome_thumbnail, "mipmap", getPackageName());
        //stesso procedimento per le immagini grandi
        nome_immagine=res.getString(7);
        IDimage = getResources().getIdentifier(nome_immagine,"mipmap",getPackageName());



        product = new Product(sku,nome,categoria,prezzo, IDthumb, colore, descrizione, IDimage);
        //aggiunta oggetto alla lista
        productList.add(product);

        //questo metodo era inserito nell'esempio ma non dovrebbe servire, lo lascio perché
        //non si sa mai
        //mAdapter.notifyDataSetChanged();

    }
    private void copyDataBase(File dbFile) throws IOException {

        //Metodo utilizzato per copiare il database da assets alla destinazione finale

        // Apertura database locale come streaming in input
        InputStream myInput = this.getAssets().open("databases/"+DATABASE_NAME);
        // percorso per il database (vuoto) appena creato
        String outFileName = String.valueOf(dbFile);
        OutputStream myOutput = new FileOutputStream(outFileName);
        // trasferimento bytes da input a  output
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

}
