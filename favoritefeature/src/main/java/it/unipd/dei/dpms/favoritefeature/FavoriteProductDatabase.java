package it.unipd.dei.dpms.favoritefeature;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/* Classe che gestisce il database e si occupa della creazione dello stesso */
@Database(entities = {FavoriteProduct.class}, version = 1)
public abstract class FavoriteProductDatabase extends RoomDatabase{

    private static FavoriteProductDatabase INSTANCE;

    //Define the DAOs that work with the database.
    //Provide an abstract "getter" method for each @Dao.
    public abstract FavoriteProductDao favoriteProductDao();

   public static FavoriteProductDatabase getDatabase(Context context){
       if(INSTANCE==null){
           INSTANCE=Room.databaseBuilder(context.getApplicationContext(),
                   FavoriteProductDatabase.class,
                   "favorite_product_database")
                   .allowMainThreadQueries() //è un rischio, bisogna vedere se crasha
                   .build();
       }
       return INSTANCE;
   }
   public static void destroyInstance(){
       INSTANCE = null;
   }

}
