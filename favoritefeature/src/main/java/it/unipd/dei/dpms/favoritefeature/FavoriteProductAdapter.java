package it.unipd.dei.dpms.favoritefeature;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import static android.content.ContentValues.TAG;

public class FavoriteProductAdapter extends RecyclerView.Adapter<FavoriteProductAdapter.MyViewHolder> {

    private List<FavoriteProduct> favoriteProductList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView sku;

        public MyViewHolder(View view) {
            super(view);
            sku = (TextView) view.findViewById(R.id.sku);
        }
    }

    public FavoriteProductAdapter(List<FavoriteProduct> favoriteProductList) {
        this.favoriteProductList = favoriteProductList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favorite_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FavoriteProduct favoriteProduct = favoriteProductList.get(position);
        Log.d(TAG, "holder at position " + position + " is " + holder);
        holder.sku.setText(favoriteProduct.getSku());
    }

    @Override
    public int getItemCount(){return favoriteProductList.size();}
}