package it.unipd.dei.dpms.favoritefeature;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

public class FavoriteListActivity extends AppCompatActivity {

    private List<FavoriteProduct> favProductList = new ArrayList<>();
    private RecyclerView fRecyclerView;
    private FavoriteProductAdapter fAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_list);

        //dobbiamo collegare questa classe con quella principale
        //così ogni volta che si schiaccia il cuore quel prodotto
        //entra in questo database

        //Bisogna ancora sistemare la barra in alto

        Intent intent = getIntent();

        fRecyclerView = findViewById(R.id.recycler_view);
        fAdapter = new FavoriteProductAdapter(favProductList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        fRecyclerView.setLayoutManager(mLayoutManager);

        fRecyclerView.setAdapter(fAdapter);


        FavoriteProductDatabase fav_db = FavoriteProductDatabase.getDatabase(this);

        //ripulisco il database prima di inizializzarlo
        //da fare sempre altrimenti crasha
        fav_db.favoriteProductDao().deleteAll();

        FavoriteProduct fav = new FavoriteProduct();


        fav.setSku("CL002A");
        fav_db.favoriteProductDao().insert(fav);
        favProductList.add(fav);
        fav.setSku("CL003A");
        insertFavProduct(fav_db,fav);
        favProductList.add(fav);
        fav.setSku("CL004A");
        insertFavProduct(fav_db,fav);
        favProductList.add(fav);
        fav.setSku("BR002A");
        insertFavProduct(fav_db,fav);
        favProductList.add(fav);
        fav.setSku("OR002A");
        insertFavProduct(fav_db,fav);
        favProductList.add(fav);
        //crea la lista con tutti i prodotti contenuti


    }
    private static FavoriteProduct insertFavProduct(final FavoriteProductDatabase db, FavoriteProduct product){
        db.favoriteProductDao().insert(product);
        return product;
    }
}
