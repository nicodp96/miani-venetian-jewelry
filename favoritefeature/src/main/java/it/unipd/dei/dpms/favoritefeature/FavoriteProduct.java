package it.unipd.dei.dpms.favoritefeature;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


/*Classe che rappresenta l'unica tabella del database, denominata "favoriteProduct", ha un'unica
  colonna "sku", contenente l'sku del prodotto aggiunto ai preferiti */

@Entity(tableName = "favoriteProduct")
public class FavoriteProduct {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "sku")
    private String sku;

    public String getSku() {
        return sku;
    }

    public void setSku(@NonNull String sku) {
        this.sku = sku;
    }

}
